import React from "react";
import { ThemeContext } from "../contexts/ThemeContext";
import { AuthContext } from "../contexts/AuthContext";

class Navbar extends React.Component {
  render() {
    return (
      <AuthContext.Consumer>
        {(authContext) => {
          return (
            <ThemeContext.Consumer>
              {(themeContext) => {
                const { isAuthenticated, toggleAuth } = authContext;
                const theme = themeContext.activeTheme();
                return (
                  <nav style={{ background: theme.ui, color: theme.syntax }}>
                    <h1 style={{ display: "inline-block" }}>Context API</h1>
                    <button onClick={toggleAuth} style={{ marginLeft: "2em" }}>
                      {isAuthenticated ? "Logout" : "Login"}
                    </button>
                    <div>
                      {isAuthenticated
                        ? "You are now logged in!"
                        : "Please log in"}
                    </div>
                    <ul>
                      <li>Home</li>
                      <li>About</li>
                      <li>Contact</li>
                    </ul>
                  </nav>
                );
              }}
            </ThemeContext.Consumer>
          );
        }}
      </AuthContext.Consumer>
    );
  }
}

export default Navbar;
